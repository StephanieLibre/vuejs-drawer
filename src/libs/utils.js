export function loadImage(src) {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = () => resolve(img);
    img.onerror = () => {
      reject(new Error(`image "${src}" not found`));
    };
    img.src = src;
  });
}
export const a = {};
