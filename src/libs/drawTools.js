import pen from '../assets/pen.svg';
import fluo from '../assets/fluo.svg';
import marker from '../assets/marker.svg';
import arrow from '../assets/arrow.svg';
import circle from '../assets/circle.svg';

export default {
  pen: {
    icon: pen,
    draw(ctx, points) {
      ctx.strokeStyle = 'red';
      ctx.lineWidth = 5;
      ctx.beginPath();
      if (points.length > 0) {
        const { x, y } = points[0];
        ctx.moveTo(x, y);
      }
      points.forEach(({ x, y }) => {
        ctx.lineTo(x, y);
      });
      ctx.stroke();
    },
  },

  fluo: {
    icon: fluo,
    draw(ctx, points) {
      ctx.strokeStyle = 'rgba(229,222,41,0.5)';
      ctx.lineWidth = 20;
      ctx.beginPath();
      if (points.length > 0) {
        const { x, y } = points[0];
        ctx.moveTo(x, y);
      }
      points.forEach(({ x, y }) => {
        ctx.lineTo(x, y);
      });
      ctx.stroke();
    },
  },
  marker: {
    icon: marker,
    draw(ctx, points) {
      ctx.strokeStyle = 'black';
      ctx.lineWidth = 25;
      ctx.beginPath();
      if (points.length > 0) {
        const { x, y } = points[0];
        ctx.moveTo(x, y);
      }
      points.forEach(({ x, y }) => {
        ctx.lineTo(x, y);
      });
      ctx.stroke();
    },
  },
  arrow: {
    icon: arrow,
    draw(ctx, points) {
      if (points.length > 1) {
        ctx.strokeStyle = 'red';
        ctx.lineWidth = 5;
        const { x: x1, y: y1 } = points[0];
        const { x: x2, y: y2 } = points[points.length - 1];

        // const radius = Math.sqrt((x2-x1)**2+(y2-y1)**2)
        /* ctx.moveTo(x1, y1)
         ctx.beginPath();
         ctx.lineTo(x1, y1)
         ctx.lineTo(x2, y2)
         ctx.stroke(); */

        ctx.beginPath();
        const headlen = 25; // length of head in pixels
        const dx = x1 - x2;
        const dy = y1 - y2;
        const angle = Math.atan2(dy, dx);
        ctx.moveTo(x1, y1);
        ctx.lineTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.moveTo(x1, y1);
        ctx.lineTo(
          x1 - headlen * Math.cos(angle - Math.PI / 6),
          y1 - headlen * Math.sin(angle - Math.PI / 6),
        );
        ctx.moveTo(x1, y1);
        ctx.lineTo(
          x1 - headlen * Math.cos(angle + Math.PI / 6),
          y1 - headlen * Math.sin(angle + Math.PI / 6),
        );
        ctx.stroke();
      }
    },
  },
  circle: {
    icon: circle,
    draw(ctx, points) {
      if (points.length > 1) {
        ctx.strokeStyle = 'red';
        ctx.lineWidth = 5;
        const { x: x1, y: y1 } = points[0];
        const { x: x2, y: y2 } = points[points.length - 1];
        const radius = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
        ctx.beginPath();
        ctx.arc(x1, y1, radius, 0, 2 * Math.PI);
        ctx.stroke();
      }
    },
  },

};
