module.exports = {
  lintOnSave: false,
  publicPath: process.env.CI ? '/vuejs-drawer' : '/',
};
